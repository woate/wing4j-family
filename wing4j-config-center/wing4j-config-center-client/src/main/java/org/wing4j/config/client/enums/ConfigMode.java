package org.wing4j.config.client.enums;

/**
 * Created by wing4j on 2017/2/10.
 * 配置模式
 */
public enum  ConfigMode {
    /**
     * 自动
     */
    AUTO,
    /**
     * 本地模式
     */
    LOCAL,
    /**
     * 服务器模式
     */
    SERVER
}
